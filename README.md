Assignment #9 - Authentication app

- As a user I want to have a protected and authorized access to my WeatherApp application

Flows described:

- This is a continuation of Weather application already done
- Integrate ASP .NET Identity into Web API
- Bootstrap DB tables it requires to work
- Add an endpoint to login, generate and get JWT token
- Seed a dummy user for test purposes
- On a frontend side all Login page (or popup) where user should enter login and password
- Make a call to Web API to a previously created endpoint
- If token has been successfully generated - show an app itself. Until user is authenticated -
don't show anything except controls required for log in process
- Add HTTP interceptor and include this token as Bearer into each call to an API
- Add at least one endpoint (or protect existing one) with Authorize attribute.

Acceptance criteria:

- If user doesn't have a token yet - don't show an app itself. Only controls required to
log in should be shown
- Allow user to enter his login and password and get authenticated
- Include obtained token into HTTP requests in headers
- At least one protected endpoint should be added (with ability to validate fact
that the protection works)

Required technical stack: .NET Core 3, Web API, Angular 8+, SQL Server, ASP .NET Identity

Advance level requirements:
- Implement Refresh token

Before starting:
- Run all migrations from the backend project to set up the Database