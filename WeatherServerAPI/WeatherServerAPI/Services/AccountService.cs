﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Services;
using WeatherCore.DTO;
using WeatherCore.Exceptions;
using WeatherCore.Models;

namespace WeatherServerAPI.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<WeatherUser> _userManager;
        private readonly SignInManager<WeatherUser> _signInManager;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;

        public AccountService(UserManager<WeatherUser> userManager, SignInManager<WeatherUser> signInManager, ITokenService tokenService, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _mapper = mapper;
        }

        public UserAuthDTO Login(UserCredentialsDTO loginCreds)
        {
            var result = _signInManager.PasswordSignInAsync(loginCreds.Username, loginCreds.Password, false, false).Result;

            if (result.Succeeded)
            {
                var weatherUser = _userManager.Users.SingleOrDefault(u => u.UserName == loginCreds.Username);
                var tokenDTO = _tokenService.BuildToken(weatherUser);
                return tokenDTO;
            }

            throw new LoginException("Invalid username or password");
        }

        public async Task<UserAuthDTO> LoginAsync(UserCredentialsDTO loginCreds)
        {
            var result = await _signInManager.PasswordSignInAsync(loginCreds.Username, loginCreds.Password, false, false);

            if (result.Succeeded)
            {
                var weatherUser = _userManager.Users.SingleOrDefault(u => u.UserName == loginCreds.Username);
                var tokenDTO = _tokenService.BuildToken(weatherUser);
                return tokenDTO;
            }

            throw new LoginException("Invalid username or password");
        }

        public UserAuthDTO Register(UserCredentialsDTO registerCreds)
        {
            var user = _mapper.Map<WeatherUser>(registerCreds);

            var result = _userManager.CreateAsync(user, registerCreds.Password).Result;

            if (result.Succeeded)
            {
                _signInManager.SignInAsync(user, false);
                var tokenDTO = _tokenService.BuildToken(user);
                return tokenDTO;
            }

            throw new RegisterException(result.Errors);
        }

        public async Task<UserAuthDTO> RegisterAsync(UserCredentialsDTO registerCreds)
        {
            var user = _mapper.Map<WeatherUser>(registerCreds);

            var result = await _userManager.CreateAsync(user, registerCreds.Password);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                var tokenDTO = _tokenService.BuildToken(user);
                return tokenDTO;
            }

            throw new RegisterException(result.Errors);
        }
    }
}
