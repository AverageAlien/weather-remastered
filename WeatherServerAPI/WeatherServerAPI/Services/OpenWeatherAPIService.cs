﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Services;
using WeatherCore.DTO;

namespace WeatherServerAPI.Services
{
    public class OpenWeatherAPIService : IWeatherDataService
    {
        private const string _openweatherURL = "https://api.openweathermap.org/data/2.5/weather";
        private readonly string _apiKey;
        private readonly IHttpClientFactory _httpClientFactory;

        public OpenWeatherAPIService(IHttpClientFactory httpFactory, IConfiguration config)
        {
            _apiKey = config.GetConnectionString("OpenweatherAPIKey");
            _httpClientFactory = httpFactory;
        }

        private string MakeRequestString(string city)
        {
            return $"{_openweatherURL}?appid={_apiKey}&q={city}";
        }

        private WeatherStatusDTO JsonToDTO(string json)
        {
            var jsonTemplate = new
            {
                weather = new[]
                    {
                        new
                        {
                            main = "",
                            icon = ""
                        }
                    },
                main = new
                {
                    temp = 0d
                },
                name = ""
            };

            var jsonObject = JsonConvert.DeserializeAnonymousType(json, jsonTemplate);

            var weatherDTO = new WeatherStatusDTO
            {
                CityName = jsonObject.name,
                Icon = jsonObject.weather[0].icon,
                Status = jsonObject.weather[0].main,
                Temperature = jsonObject.main.temp,
                Date = DateTime.UtcNow
            };

            return weatherDTO;
        }

        public WeatherStatusDTO GetWeather(string CityName)
        {
            using var client = _httpClientFactory.CreateClient();
            var request = new HttpRequestMessage(HttpMethod.Get, MakeRequestString(CityName));
            var response = client.SendAsync(request).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseStr = response.Content.ReadAsStringAsync().Result;

                var weatherDTO = JsonToDTO(responseStr);

                return weatherDTO;
            }
            else
            {
                throw new HttpRequestException(response.StatusCode.ToString());
            }
        }

        public async Task<WeatherStatusDTO> GetWeatherAsync(string CityName)
        {
            using var client = _httpClientFactory.CreateClient();
            var request = new HttpRequestMessage(HttpMethod.Get, MakeRequestString(CityName));
            var response = await client.SendAsync(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseStr = await response.Content.ReadAsStringAsync();

                var weatherDTO = JsonToDTO(responseStr);

                return weatherDTO;
            }
            else
            {
                throw new HttpRequestException(response.StatusCode.ToString());
            }
        }
    }
}
