﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WeatherCore.Abstractions;
using WeatherCore.Abstractions.Services;
using WeatherCore.DTO;
using WeatherCore.Models;

namespace WeatherServerAPI.Services
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _config;
        private readonly IUnitOfWork _unitOfWork;

        public TokenService(IConfiguration config, IUnitOfWork unitOfWork)
        {
            _config = config;
            _unitOfWork = unitOfWork;
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }

        private UserAuthDTO MakeToken(WeatherUser user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var tokenclaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName)
            };
            var expirationDate = DateTime.UtcNow.AddMinutes(_config.GetValue<int>("Jwt:TokenLifetime"));
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                expires: expirationDate,
                signingCredentials: creds,
                claims: tokenclaims);
            var tokenStr = new JwtSecurityTokenHandler().WriteToken(token);

            var refreshToken = GenerateRefreshToken();

            return new UserAuthDTO
            {
                Username = user.UserName,
                ExpirationDate = expirationDate,
                Token = tokenStr,
                RefreshToken = refreshToken
            };
        }

        private ClaimsPrincipal GetClaimsFromExpiredToken(string token)
        {
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = false,
                ValidIssuer = _config["Jwt:Issuer"],
                ValidAudience = _config["Jwt:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]))
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, validationParameters, out SecurityToken securityToken);

            var jwtToken = securityToken as JwtSecurityToken;

            if (jwtToken == null || !jwtToken.Header.Alg.Equals(
                SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }

            return principal;
        }

        public UserAuthDTO BuildToken(WeatherUser user)
        {
            var authDTO = MakeToken(user);

            var refreshEntity = new RefreshToken
            {
                Token = authDTO.RefreshToken,
                Expiration = DateTime.UtcNow.AddMinutes(_config.GetValue<int>("Jwt:RefreshLifetime")),
                WeatherUser = user,
                WeatherUserId = user.Id
            };
            _unitOfWork.RefreshRepository.Add(refreshEntity);
            _unitOfWork.SaveChanges();

            return authDTO;
        }

        public async Task<UserAuthDTO> BuildTokenAsync(WeatherUser user)
        {
            var authDTO = MakeToken(user);

            var refreshEntity = new RefreshToken
            {
                Token = authDTO.RefreshToken,
                Expiration = DateTime.UtcNow.AddMinutes(_config.GetValue<int>("Jwt:RefreshLifetime")),
                WeatherUser = user,
                WeatherUserId = user.Id
            };
            await _unitOfWork.RefreshRepository.AddAsync(refreshEntity);
            await _unitOfWork.SaveChangesAsync();

            return authDTO;
        }

        public UserAuthDTO RefreshExpiredToken(TokenRefreshRequestDTO refreshDTO)
        {
            var principal = GetClaimsFromExpiredToken(refreshDTO.ExpiredToken);
            var username = principal.Identity.Name;
            var refreshToken = refreshDTO.RefreshToken;
            var validRefreshToken = _unitOfWork.RefreshRepository
                .GetAll()
                .Where(t => t.Expiration > DateTime.UtcNow)
                .FirstOrDefault(t => t.Token == refreshToken && t.WeatherUser.UserName == username);
            if (validRefreshToken == null)
            {
                throw new SecurityTokenException("Invalid refresh token");
            }
            var freshAuthDTO = MakeToken(validRefreshToken.WeatherUser);

            validRefreshToken.Expiration = DateTime.UtcNow.AddMinutes(_config.GetValue<int>("Jwt:RefreshLifetime"));
            validRefreshToken.Token = GenerateRefreshToken();

            _unitOfWork.SaveChanges();

            return freshAuthDTO;
        }

        public async Task<UserAuthDTO> RefreshExpiredTokenAsync(TokenRefreshRequestDTO refreshDTO)
        {
            var principal = GetClaimsFromExpiredToken(refreshDTO.ExpiredToken);
            var username = principal.Identity.Name;
            var refreshToken = refreshDTO.RefreshToken;
            var validRefreshToken = await _unitOfWork.RefreshRepository
                .GetAll()
                .Where(t => t.Expiration > DateTime.UtcNow)
                .FirstOrDefaultAsync(t => t.Token == refreshToken && t.WeatherUser.UserName == username);
            if (validRefreshToken == null)
            {
                throw new SecurityTokenException("Invalid refresh token");
            }
            var freshAuthDTO = MakeToken(validRefreshToken.WeatherUser);

            validRefreshToken.Expiration = DateTime.UtcNow.AddMinutes(_config.GetValue<int>("Jwt:RefreshLifetime"));
            validRefreshToken.Token = freshAuthDTO.RefreshToken;

            await _unitOfWork.SaveChangesAsync();

            return freshAuthDTO;
        }
    }
}
