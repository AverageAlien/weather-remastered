﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WeatherCore.Abstractions;
using WeatherCore.Abstractions.Services;
using WeatherCore.DTO;
using WeatherCore.Models;

namespace WeatherServerAPI.Services
{
    public class WeatherHistoryService : IWeatherHistoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public WeatherHistoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public UserDTO GetWeatherHistory(int userId)
        {
            var user = _unitOfWork.UserRepository.GetById(userId);

            var historyDTO = _mapper.Map<UserDTO>(user);

            historyDTO.WeatherHistory = historyDTO.WeatherHistory.OrderByDescending(w => w.Date);

            return historyDTO;
        }

        public async Task<UserDTO> GetWeatherHistoryAsync(int userId)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(userId);

            var historyDTO = _mapper.Map<UserDTO>(user);

            historyDTO.WeatherHistory = historyDTO.WeatherHistory.OrderByDescending(w => w.Date);

            return historyDTO;
        }

        public UserDTO GetWeatherHistory(string username)
        {
            var user = _unitOfWork.UserRepository.GetAll().Single(u => u.UserName == username);

            var historyDTO = _mapper.Map<UserDTO>(user);

            historyDTO.WeatherHistory = historyDTO.WeatherHistory.OrderByDescending(w => w.Date);

            return historyDTO;
        }

        public async Task<UserDTO> GetWeatherHistoryAsync(string username)
        {
            var user = await _unitOfWork.UserRepository.GetAll().SingleAsync(u => u.UserName == username);

            var historyDTO = _mapper.Map<UserDTO>(user);

            historyDTO.WeatherHistory = historyDTO.WeatherHistory.OrderByDescending(w => w.Date);

            return historyDTO;
        }

        public void AddEntry(string username, WeatherStatusDTO weatherStatus)
        {
            var user = _unitOfWork.UserRepository.GetAll().Single(u => u.UserName == username);

            var statusEntry = _mapper.Map<WeatherStatus>(weatherStatus);

            statusEntry.WeatherUser = user;
            statusEntry.WeatherUserId = user.Id;

            _unitOfWork.StatusRepository.Add(statusEntry);
            _unitOfWork.SaveChanges();
        }

        public async Task AddEntryAsync(string username, WeatherStatusDTO weatherStatus)
        {
            var user = await _unitOfWork.UserRepository.GetAll().SingleAsync(u => u.UserName == username);

            var statusEntry = _mapper.Map<WeatherStatus>(weatherStatus);

            statusEntry.WeatherUser = user;
            statusEntry.WeatherUserId = user.Id;

            await _unitOfWork.StatusRepository.AddAsync(statusEntry);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
