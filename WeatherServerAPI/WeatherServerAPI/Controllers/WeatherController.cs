﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Services;

namespace WeatherServerAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("AllowAny")]
    [ApiController]
    public class WeatherController : ControllerBase
    {

        private readonly IWeatherDataService _weatherDataService;
        private readonly IWeatherHistoryService _weatherHistoryService;

        public WeatherController(IWeatherDataService weatherDataService, IWeatherHistoryService weatherHistoryService)
        {
            _weatherDataService = weatherDataService;
            _weatherHistoryService = weatherHistoryService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string city)
        {
            try
            {
                var resultWeather = await _weatherDataService.GetWeatherAsync(city);

                var username = HttpContext.User.FindFirst(ClaimTypes.Name).Value;

                await _weatherHistoryService.AddEntryAsync(username, resultWeather);

                return Ok(resultWeather);
            }
            catch (HttpRequestException e)
            {
                if (e.Message == HttpStatusCode.NotFound.ToString())
                {
                    return NotFound();
                }
                return StatusCode(500, new { error = e.Message });
            }
        }

    }
}