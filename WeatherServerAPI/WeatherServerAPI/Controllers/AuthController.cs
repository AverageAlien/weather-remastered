﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Services;
using WeatherCore.DTO;
using WeatherCore.Exceptions;

namespace WeatherServerAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAny")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly ITokenService _tokenService;

        public AuthController(IAccountService accountService, ITokenService tokenService)
        {
            _accountService = accountService;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody]UserCredentialsDTO loginDTO)
        {
            try
            {
                var token = await _accountService.LoginAsync(loginDTO);
                return Ok(token);
            }
            catch (LoginException e)
            {
                return Unauthorized(e.Message);
            }
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody]UserCredentialsDTO registerDTO)
        {
            try
            {
                var token = await _accountService.RegisterAsync(registerDTO);
                return Ok(token);
            }
            catch (RegisterException e)
            {
                return BadRequest(e.Errors);
            }
        }

        [HttpPost]
        [Route("Refresh")]
        public async Task<IActionResult> RefreshToken([FromBody]TokenRefreshRequestDTO refreshDTO)
        {
            try
            {
                var token = await _tokenService.RefreshExpiredTokenAsync(refreshDTO);
                return Ok(token);
            }
            catch (SecurityTokenException e)
            {
                return Unauthorized(e.Message);
            }
        }
    }
}