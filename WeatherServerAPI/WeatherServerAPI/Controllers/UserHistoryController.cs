﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Services;

namespace WeatherServerAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("AllowAny")]
    [ApiController]
    public class UserHistoryController : ControllerBase
    {
        private readonly IWeatherHistoryService _weatherHistoryService;

        public UserHistoryController(IWeatherHistoryService weatherDataService)
        {
            _weatherHistoryService = weatherDataService;
        }

        [HttpGet]
        public async Task<IActionResult> GetHistory()
        {
            var username = HttpContext.User.FindFirst(ClaimTypes.Name).Value;

            var userHistory = await _weatherHistoryService.GetWeatherHistoryAsync(username);

            return Ok(userHistory);
        }
    }
}