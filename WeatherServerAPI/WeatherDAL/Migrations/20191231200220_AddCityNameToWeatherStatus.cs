﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeatherDAL.Migrations
{
    public partial class AddCityNameToWeatherStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CityName",
                table: "WeatherRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CityName",
                table: "WeatherRequests");
        }
    }
}
