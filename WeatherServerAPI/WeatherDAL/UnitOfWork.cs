﻿using System;
using System.Threading.Tasks;
using WeatherCore.Abstractions;
using WeatherCore.Abstractions.Repositories;
using WeatherDAL.Repositories;

namespace WeatherDAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private WeatherDbContext _db;

        private bool disposed = false;

        private IWeatherUserRepository _weatherUserRepository;
        private IWeatherStatusRepository _weatherStatusRepository;
        private IRefreshTokenRepository _refreshTokenRepository;

        public IWeatherUserRepository UserRepository => _weatherUserRepository ??= new WeatherUserRepository(_db);
        public IWeatherStatusRepository StatusRepository => _weatherStatusRepository ??= new WeatherStatusRepository(_db);
        public IRefreshTokenRepository RefreshRepository => _refreshTokenRepository ??= new RefreshTokenRepository(_db);

        public UnitOfWork(WeatherDbContext weatherDb)
        {
            _db = weatherDb;
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                    _db = null;
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
    }
}
