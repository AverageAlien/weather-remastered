﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WeatherCore.Abstractions;
using WeatherCore.Abstractions.Repositories;

namespace WeatherDAL.Repositories
{
    public class BaseRepository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        protected readonly WeatherDbContext _db;

        public BaseRepository(WeatherDbContext WeatherDb)
        {
            _db = WeatherDb;
        }

        public virtual TEntity GetById(TKey Id)
        {
            return _db.Set<TEntity>().Find(Id);
        }

        public virtual async Task<TEntity> GetByIdAsync(TKey Id)
        {
            return await _db.Set<TEntity>().FindAsync(Id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _db.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            _db.Set<TEntity>().Add(entity);
            return entity;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            await _db.Set<TEntity>().AddAsync(entity);
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            return GetById(entity.Id);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            return await GetByIdAsync(entity.Id);
        }

        public void Delete(TKey Id)
        {
            var Target = GetById(Id);
            _db.Set<TEntity>().Remove(Target);
        }

        public async Task DeleteAsync(TKey Id)
        {
            var Target = await GetByIdAsync(Id);
            _db.Set<TEntity>().Remove(Target);
        }
    }
}
