﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Repositories;
using WeatherCore.Models;

namespace WeatherDAL.Repositories
{
    public class RefreshTokenRepository : BaseRepository<RefreshToken, int>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(WeatherDbContext weatherDb) : base(weatherDb) { }

        public override RefreshToken GetById(int Id)
        {
            return _db.Set<RefreshToken>()
                .Include(t => t.WeatherUser)
                .FirstOrDefault(t => t.Id == Id);
        }

        public override Task<RefreshToken> GetByIdAsync(int Id)
        {
            return _db.Set<RefreshToken>()
                .Include(t => t.WeatherUser)
                .FirstOrDefaultAsync(t => t.Id == Id);
        }

        public override IQueryable<RefreshToken> GetAll()
        {
            return _db.Set<RefreshToken>()
                .Include(t => t.WeatherUser);
        }
    }
}
