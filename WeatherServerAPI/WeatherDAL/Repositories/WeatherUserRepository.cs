﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Repositories;
using WeatherCore.Models;

namespace WeatherDAL.Repositories
{
    public class WeatherUserRepository : BaseRepository<WeatherUser, int>, IWeatherUserRepository
    {
        public WeatherUserRepository(WeatherDbContext weatherDb) : base(weatherDb) { }

        public override WeatherUser GetById(int Id)
        {
            return _db.Set<WeatherUser>()
                .Include(u => u.WeatherStatuses)
                .FirstOrDefault(u => u.Id == Id);
        }

        public override Task<WeatherUser> GetByIdAsync(int Id)
        {
            return _db.Set<WeatherUser>()
                .Include(u => u.WeatherStatuses)
                .FirstOrDefaultAsync(u => u.Id == Id);
        }

        public override IQueryable<WeatherUser> GetAll()
        {
            return _db.Set<WeatherUser>()
                .Include(u => u.WeatherStatuses);
        }
    }
}
