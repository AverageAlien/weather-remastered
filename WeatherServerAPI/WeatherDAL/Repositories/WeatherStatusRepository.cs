﻿using WeatherCore.Abstractions.Repositories;
using WeatherCore.Models;

namespace WeatherDAL.Repositories
{
    public class WeatherStatusRepository : BaseRepository<WeatherStatus, int>, IWeatherStatusRepository
    {
        public WeatherStatusRepository(WeatherDbContext weatherDb) : base(weatherDb) { }
    }
}
