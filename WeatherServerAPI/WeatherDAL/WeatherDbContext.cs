﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WeatherCore.Models;

namespace WeatherDAL
{
    public class WeatherDbContext : IdentityDbContext<WeatherUser, IdentityRole<int>, int>
    {

        public DbSet<WeatherStatus> WeatherRequests { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public WeatherDbContext(DbContextOptions<WeatherDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<WeatherStatus>(status =>
            {
                status.Property(s => s.Id).ValueGeneratedOnAdd();

                status.HasOne(s => s.WeatherUser)
                .WithMany(u => u.WeatherStatuses)
                .HasForeignKey(s => s.WeatherUserId);
            });

            builder.Entity<RefreshToken>(token =>
            {
                token.Property(t => t.Id).ValueGeneratedOnAdd();

                token.HasOne(t => t.WeatherUser)
                .WithMany(u => u.RefreshTokens)
                .HasForeignKey(t => t.WeatherUserId);
            });
        }
    }
}
