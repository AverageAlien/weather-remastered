﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using WeatherCore.Abstractions;
using WeatherCore.Models;

namespace WeatherDAL
{
    public static class Seeder
    {
        public static void Seed(IUnitOfWork unitOfWork, UserManager<WeatherUser> userManager)
        {
            if (userManager.Users.Count() > 0) return;
            var seededUser = new WeatherUser
            {
                UserName = "DefaultUser",
                PasswordHash = "1234"
            };
            var result = userManager.CreateAsync(seededUser, seededUser.PasswordHash).Result;

            if (result.Succeeded)
            {
                var seededWeather = new WeatherStatus
                {
                    CityName = "Mukachevo",
                    Temperature = 283.5,
                    Icon = "01n",
                    Status = "Sunny",
                    Date = DateTime.UtcNow,
                    WeatherUser = seededUser,
                    WeatherUserId = seededUser.Id
                };

                unitOfWork.StatusRepository.Add(seededWeather);
                unitOfWork.SaveChanges();
            }
        }
    }
}
