﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using WeatherCore.Abstractions;

namespace WeatherCore.Models
{
    public class WeatherUser : IdentityUser<int>, IEntity<int>
    {
        public IEnumerable<WeatherStatus> WeatherStatuses { get; set; }
        public IEnumerable<RefreshToken> RefreshTokens { get; set; }
    }
}
