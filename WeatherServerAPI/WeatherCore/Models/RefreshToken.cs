﻿using System;
using WeatherCore.Abstractions;

namespace WeatherCore.Models
{
    public class RefreshToken : IEntity<int>
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public int WeatherUserId { get; set; }
        public WeatherUser WeatherUser { get; set; }
    }
}
