﻿using System;
using WeatherCore.Abstractions;

namespace WeatherCore.Models
{
    public class WeatherStatus : IEntity<int>
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public double Temperature { get; set; }
        public string Status { get; set; }
        public string Icon { get; set; }
        public DateTime Date { get; set; }
        public int WeatherUserId { get; set; }
        public WeatherUser WeatherUser { get; set; }
    }
}
