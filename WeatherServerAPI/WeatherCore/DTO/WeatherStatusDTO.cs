﻿using System;

namespace WeatherCore.DTO
{
    public class WeatherStatusDTO
    {
        public string CityName { get; set; }
        public double Temperature { get; set; }
        public string Status { get; set; }
        public string Icon { get; set; }
        public DateTime Date { get; set; }
    }
}
