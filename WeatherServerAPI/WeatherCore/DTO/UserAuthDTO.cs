﻿using System;

namespace WeatherCore.DTO
{
    public class UserAuthDTO
    {
        public string Token { get; set; }
        public string Username { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string RefreshToken { get; set; }
    }
}
