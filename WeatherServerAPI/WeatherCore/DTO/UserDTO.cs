﻿using System.Collections.Generic;

namespace WeatherCore.DTO
{
    public class UserDTO
    {
        public string Username { get; set; }
        public IEnumerable<WeatherStatusDTO> WeatherHistory { get; set; }
    }
}
