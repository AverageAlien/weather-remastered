﻿namespace WeatherCore.DTO
{
    public class TokenRefreshRequestDTO
    {
        public string ExpiredToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
