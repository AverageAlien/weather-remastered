﻿using System.ComponentModel.DataAnnotations;

namespace WeatherCore.DTO
{
    public class UserCredentialsDTO
    {
        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Username { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Password { get; set; }
    }
}
