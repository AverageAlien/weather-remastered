﻿using System.Threading.Tasks;
using WeatherCore.DTO;

namespace WeatherCore.Abstractions.Services
{
    public interface IWeatherHistoryService
    {
        UserDTO GetWeatherHistory(int userId);
        Task<UserDTO> GetWeatherHistoryAsync(int userId);
        UserDTO GetWeatherHistory(string username);
        Task<UserDTO> GetWeatherHistoryAsync(string username);
        void AddEntry(string username, WeatherStatusDTO weatherStatus);
        Task AddEntryAsync(string username, WeatherStatusDTO weatherStatus);
    }
}
