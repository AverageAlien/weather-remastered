﻿using System.Threading.Tasks;
using WeatherCore.DTO;

namespace WeatherCore.Abstractions.Services
{
    public interface IAccountService
    {
        UserAuthDTO Login(UserCredentialsDTO loginCreds);
        Task<UserAuthDTO> LoginAsync(UserCredentialsDTO loginCreds);
        UserAuthDTO Register(UserCredentialsDTO registerCreds);
        Task<UserAuthDTO> RegisterAsync(UserCredentialsDTO registerCreds);
    }
}
