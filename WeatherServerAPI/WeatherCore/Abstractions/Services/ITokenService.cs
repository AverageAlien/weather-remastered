﻿using System.Threading.Tasks;
using WeatherCore.DTO;
using WeatherCore.Models;

namespace WeatherCore.Abstractions.Services
{
    public interface ITokenService
    {
        UserAuthDTO BuildToken(WeatherUser user);
        Task<UserAuthDTO> BuildTokenAsync(WeatherUser user);
        UserAuthDTO RefreshExpiredToken(TokenRefreshRequestDTO refreshDTO);
        Task<UserAuthDTO> RefreshExpiredTokenAsync(TokenRefreshRequestDTO refreshDTO);
    }
}
