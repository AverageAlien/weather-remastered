﻿using System.Threading.Tasks;
using WeatherCore.DTO;

namespace WeatherCore.Abstractions.Services
{
    public interface IWeatherDataService
    {
        WeatherStatusDTO GetWeather(string City);
        Task<WeatherStatusDTO> GetWeatherAsync(string City);
    }
}
