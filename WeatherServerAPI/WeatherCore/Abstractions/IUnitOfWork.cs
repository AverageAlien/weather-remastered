﻿using System;
using System.Threading.Tasks;
using WeatherCore.Abstractions.Repositories;

namespace WeatherCore.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        IWeatherUserRepository UserRepository { get; }
        IWeatherStatusRepository StatusRepository { get; }
        IRefreshTokenRepository RefreshRepository { get; }

        void SaveChanges();
        Task SaveChangesAsync();
    }
}
