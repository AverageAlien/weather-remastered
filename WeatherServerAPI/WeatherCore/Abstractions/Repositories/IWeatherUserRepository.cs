﻿using WeatherCore.Models;

namespace WeatherCore.Abstractions.Repositories
{
    public interface IWeatherUserRepository : IRepository<WeatherUser, int>
    {
    }
}
