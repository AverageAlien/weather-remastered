﻿using WeatherCore.Models;

namespace WeatherCore.Abstractions.Repositories
{
    public interface IWeatherStatusRepository : IRepository<WeatherStatus, int>
    {
    }
}
