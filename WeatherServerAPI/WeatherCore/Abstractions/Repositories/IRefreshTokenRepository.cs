﻿using WeatherCore.Models;

namespace WeatherCore.Abstractions.Repositories
{
    public interface IRefreshTokenRepository : IRepository<RefreshToken, int>
    {
    }
}
