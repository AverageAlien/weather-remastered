﻿using System.Linq;
using System.Threading.Tasks;

namespace WeatherCore.Abstractions.Repositories
{
    public interface IRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        TEntity GetById(TKey Id);
        IQueryable<TEntity> GetAll();
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TKey Id);

        Task<TEntity> GetByIdAsync(TKey Id);
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeleteAsync(TKey Id);
    }
}
