﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace WeatherCore.Exceptions
{
    public class RegisterException : Exception
    {
        public IEnumerable<IdentityError> Errors { get; set; }

        public RegisterException(IEnumerable<IdentityError> identityErrors)
        {
            Errors = identityErrors;
        }
    }
}
