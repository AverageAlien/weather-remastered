﻿using AutoMapper;
using WeatherCore.DTO;
using WeatherCore.Models;

namespace WeatherCore
{
    public class EntityMapper : Profile
    {
        public EntityMapper()
        {
            CreateMap<WeatherUser, UserDTO>()
                .ForMember(
                    dto => dto.Username,
                    c => c.MapFrom(
                        u => u.UserName
                    ))
                .ForMember(
                    dto => dto.WeatherHistory,
                    c => c.MapFrom(
                        u => u.WeatherStatuses
                    )).ReverseMap();

            CreateMap<WeatherStatus, WeatherStatusDTO>().ReverseMap();

            CreateMap<WeatherUser, UserCredentialsDTO>()
                .ForMember(
                    dto => dto.Password,
                    c => c.MapFrom(
                        u => u.PasswordHash
                    ))
                .ReverseMap();
        }
    }
}
