import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './components/app/app.component';
import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { MainViewComponent } from './components/weather-page/main-view/main-view.component';
import { InputWindowComponent } from './components/weather-page/input-window/input-window.component';
import { WeatherWindowComponent } from './components/weather-page/weather-window/weather-window.component';
import { ErrorWindowComponent } from './components/weather-page/error-window/error-window.component';
import { PageRoutingModule } from './page-routing.module';
import { WelcomeWindowComponent } from './components/landing-page/welcome-window/welcome-window.component';
import { NotFoundWindowComponent } from './components/landing-page/not-found-window/not-found-window.component';
import { AuthComponent } from './components/auth/auth.component';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { UserHistoryComponent } from './components/user-history/user-history.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderBarComponent,
    MainViewComponent,
    InputWindowComponent,
    WeatherWindowComponent,
    ErrorWindowComponent,
    WelcomeWindowComponent,
    NotFoundWindowComponent,
    AuthComponent,
    UserHistoryComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    PageRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
