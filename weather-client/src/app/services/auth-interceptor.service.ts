import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { exhaustMap, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AuthenticationService } from './authentication.service';
import { UserData } from '../models/userData.model';


@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthenticationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.userData.pipe(
      take(1),
      exhaustMap((user: UserData) => {
        if (user) {
          const modifiedReq = req.clone({
            headers: req.headers
              .set('Authorization', 'Bearer ' + user.token)
          });
          return next.handle(modifiedReq);
        }
        return next.handle(req);
      })
    );
  }
}
