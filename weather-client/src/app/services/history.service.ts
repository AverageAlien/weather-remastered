import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Weather, WeatherResponse } from '../interfaces/weather.interface';


@Injectable({ providedIn: 'root' })
export class HistoryService {
  userHistory: Weather[] = [];
  username: string = null;

  constructor(private http: HttpClient) { }

  loadHistory() {
    const queryStr = 'api/userhistory';
    this.http.get(queryStr).subscribe(
      (res: {
        username: string;
        weatherHistory: WeatherResponse[];
      }) => {
        this.username = res.username;

        this.userHistory = res.weatherHistory.map(w => {
          let status: Weather;

          status = {
            icon: w.icon,
            temp: Math.round(w.temperature - 273.15),
            town: w.cityName,
            weatherType: w.weatherStatus,
            date: new Date(w.date + 'Z')
          };

          return status;
        });
      }
    );
  }

  resetState() {
    this.userHistory = [];
    this.username = null;
  }
}
