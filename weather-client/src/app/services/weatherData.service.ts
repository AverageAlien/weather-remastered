import { Weather } from '../interfaces/weather.interface';
import { Injectable } from "@angular/core";


@Injectable()
export class WeatherDataService {
  weatherStatus: Weather = null;
  error: number = null;

  resetStatus() {
    this.weatherStatus = null;
    this.error = null;
  }
}
