import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { LoginCredentials } from '../interfaces/credentials.interface';
import { TokenResponse } from '../interfaces/tokenResponse.interface';
import { UserData } from '../models/userData.model';
import { RefreshToken } from '../interfaces/refreshToken.interface';



@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  userData = new BehaviorSubject<UserData>(null);

  constructor(private http: HttpClient, private router: Router) { }

  login(credentials: LoginCredentials): Observable<TokenResponse> {
    const queryStr = 'api/auth/login';
    return this.http.post<TokenResponse>(queryStr, credentials)
    .pipe(
      tap(
        (resData: TokenResponse) => {
          const user = new UserData(resData.username, resData.token, new Date(resData.expirationDate), resData.refreshToken);
          this.userData.next(user);
          localStorage.setItem('userData', JSON.stringify(user));
        }
      )
    );
  }

  autoLogin() {
    const userSnapshot: {
      username: string;
      token: string;
      expirationDate: string;
      refreshToken: string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userSnapshot) {
      return;
    }

    const loadedUser = new UserData(
      userSnapshot.username,
      userSnapshot.token,
      new Date(userSnapshot.expirationDate),
      userSnapshot.refreshToken
    );
    if (!loadedUser.expired) {
      this.userData.next(loadedUser);
      return;
    }

    const refreshRequest: RefreshToken = {
      expiredToken: loadedUser.token,
      refreshToken: loadedUser.refreshToken
    };

    this.attemptRefresh(refreshRequest).subscribe(
      (freshUser: UserData) => {
        this.userData.next(freshUser);
        localStorage.setItem('userData', JSON.stringify(freshUser));
      }
    );
  }

  logout() {
    this.userData.next(null);
    this.router.navigate(['/login']);
    localStorage.removeItem('userData');
  }

  attemptRefresh(refreshRequest: RefreshToken): Observable<UserData> {
    const queryStr = 'api/auth/refresh';

    return this.http.post(queryStr, refreshRequest).pipe(
      map(
        (resData: TokenResponse) => {
          const freshUser = new UserData(
            resData.username,
            resData.token,
            new Date(resData.expirationDate),
            resData.refreshToken
          );
          return freshUser;
        }
      )
    );
  }

}
