import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

import { WeatherResponse } from '../interfaces/weather.interface';
import { WeatherDataService } from './weatherData.service';
import { AuthenticationService } from './authentication.service';
import { UserData } from '../models/userData.model';
import { RefreshToken } from '../interfaces/refreshToken.interface';


@Injectable()
export class WeatherRequestService {
  constructor(
    private http: HttpClient,
    private dataService: WeatherDataService,
    private authService: AuthenticationService
  ) { }

  getCityWeather(city: string) {
    const queryStr = `/api/weather?city=${city}`;

    this.http.get(queryStr).subscribe(
      (response: WeatherResponse) => {
        this.dataService.weatherStatus = {
          town: response.cityName,
          temp: Math.round(response.temperature - 273.15),
          weatherType: response.weatherStatus,
          icon: response.icon,
          date: new Date(response.date)
        };
      },

      (error: HttpErrorResponse) => {
        if (error.headers.has('token-expired')) {
          this.authService.userData.pipe(take(1)).subscribe(

            (user: UserData) => {
              const refreshRequest: RefreshToken = {
                expiredToken: user.token,
                refreshToken: user.refreshToken
              };

              this.authService.attemptRefresh(refreshRequest).subscribe(

                (freshUser: UserData) => {
                  this.authService.userData.next(freshUser);
                  localStorage.setItem('userData', JSON.stringify(freshUser));

                  this.http.get(queryStr).subscribe(

                    (response: WeatherResponse) => {
                      this.dataService.weatherStatus = {
                        town: response.cityName,
                        temp: Math.round(response.temperature - 273.15),
                        weatherType: response.weatherStatus,
                        icon: response.icon,
                        date: new Date(response.date)
                      };
                    },
                    (secondTryError: HttpErrorResponse) => {
                      this.dataService.error = secondTryError.status;
                    }
                  );
                },
                (refreshError: HttpErrorResponse) => {
                  this.dataService.error = refreshError.status;
                }
              );
            }
          );
        } else {
          this.dataService.error = error.status;
        }
      },
      () => {
        this.dataService.error = null;
      }
    );
  }
}
