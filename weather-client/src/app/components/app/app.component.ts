import { Component, OnInit } from '@angular/core';

import { WeatherRequestService } from 'src/app/services/weatherRequest.service';
import { WeatherDataService } from 'src/app/services/weatherData.service';
import { AuthenticationService } from 'src/app/services/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ WeatherRequestService, WeatherDataService ]
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.autoLogin();
  }
}
