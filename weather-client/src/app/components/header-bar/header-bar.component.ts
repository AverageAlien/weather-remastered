import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit, OnDestroy {
  isAuthenticated = false;
  username: string = null;
  private userSub: Subscription;

  constructor(private authService: AuthenticationService, public router: Router) { }

  ngOnInit() {
    this.userSub = this.authService.userData.subscribe(user => {
      this.isAuthenticated = !!user;
      if (this.isAuthenticated) {
        this.username = user.username;
      } else {
        this.username = null;
      }
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }
}
