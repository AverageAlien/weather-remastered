import { Component } from '@angular/core';


@Component({
  selector: 'app-not-found-window',
  templateUrl: './not-found-window.component.html',
  styleUrls: ['./not-found-window.component.scss']
})
export class NotFoundWindowComponent { }
