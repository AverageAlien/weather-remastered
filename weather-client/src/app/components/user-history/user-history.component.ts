import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HistoryService } from 'src/app/services/history.service';


@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.scss']
})
export class UserHistoryComponent implements OnInit, OnDestroy {
  constructor(private authService: AuthenticationService, public historyService: HistoryService) { }

  ngOnInit() {
    this.historyService.loadHistory();
  }

  ngOnDestroy() {
    this.historyService.resetState();
  }

  formatDate(date: Date): string {
    return date.toLocaleString('en-US', {
      weekday: 'long',
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour12: false,
      hour: 'numeric',
      minute: 'numeric'
    });
  }
}
