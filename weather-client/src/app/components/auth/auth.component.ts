import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { LoginCredentials } from 'src/app/interfaces/credentials.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TokenResponse } from 'src/app/interfaces/tokenResponse.interface';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  error: string = null;

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(20)])
    });
  }

  onLoginSubmit() {
    const credentials: LoginCredentials = {
      username: this.loginForm.value['username'],
      password: this.loginForm.value['password']
    };

    this.authService.login(credentials).subscribe(
      (response: TokenResponse) => {
        this.error = null;
        this.router.navigate(['/']);
      },
      (errorResponse: HttpErrorResponse) => {
        this.error = errorResponse.error;
      }
    );
  }
}
