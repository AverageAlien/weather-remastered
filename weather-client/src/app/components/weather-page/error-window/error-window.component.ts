import { Component } from '@angular/core';

import { WeatherDataService } from 'src/app/services/weatherData.service';


@Component({
  selector: 'app-error-window',
  templateUrl: './error-window.component.html',
  styleUrls: ['./error-window.component.scss']
})
export class ErrorWindowComponent {
  constructor(private weatherData: WeatherDataService) { }

  onResetButton() {
    this.weatherData.resetStatus();
  }

  getErrorString(): string {
    switch (this.weatherData.error) {
      case 400:
        return 'Bad request';
      case 401:
        return 'You are not logged in';
      case 404:
        return 'City not found';
      case 500:
        return 'Server error';
      default:
        return 'Unknown error';
    }
  }
}
