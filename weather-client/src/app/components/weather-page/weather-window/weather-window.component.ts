import { Component } from '@angular/core';

import { WeatherDataService } from 'src/app/services/weatherData.service';


@Component({
  selector: 'app-weather-window',
  templateUrl: './weather-window.component.html',
  styleUrls: ['./weather-window.component.scss']
})
export class WeatherWindowComponent {
  constructor(public weather: WeatherDataService) { }

  onResetButton() {
    this.weather.resetStatus();
  }
}
