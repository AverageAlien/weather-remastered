import { Component, OnDestroy } from '@angular/core';

import { WeatherDataService } from 'src/app/services/weatherData.service';


@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent {
  constructor(public weather: WeatherDataService) { }
}
