import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { WeatherRequestService } from 'src/app/services/weatherRequest.service';
import { CityValidators } from 'src/app/validators/city-names.validator';


@Component({
  selector: 'app-input-window',
  templateUrl: './input-window.component.html',
  styleUrls: ['./input-window.component.scss']
})
export class InputWindowComponent implements OnInit {
  cityForm: FormGroup;

  constructor(private weather: WeatherRequestService) { }

  ngOnInit() {
    this.cityForm = new FormGroup({
      'city': new FormControl(null, [Validators.required, CityValidators.RestrictDigits.bind(this)])
    });
  }

  onCitySend() {
    this.weather.getCityWeather(this.cityForm.value['city']);
  }
}
