import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainViewComponent } from './components/weather-page/main-view/main-view.component';
import { WelcomeWindowComponent } from './components/landing-page/welcome-window/welcome-window.component';
import { NotFoundWindowComponent } from './components/landing-page/not-found-window/not-found-window.component';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuard } from './routerGuards/auth.guard';
import { UserHistoryComponent } from './components/user-history/user-history.component';


const appRoutes: Routes = [
  { path: '', component: WelcomeWindowComponent, canActivate: [AuthGuard] },
  { path: 'weather', component: MainViewComponent, canActivate: [AuthGuard] },
  { path: 'history', component: UserHistoryComponent, canActivate: [AuthGuard] },
  { path: 'login', component: AuthComponent },
  { path: 'notfound', component: NotFoundWindowComponent },
  { path: '**', redirectTo: '/notfound' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PageRoutingModule { }
