export class UserData {
  constructor(
    public username: string,
    public token: string,
    public expirationDate: Date,
    public refreshToken: string
  ) { }

  get expired(): boolean {
    return (!this.expirationDate || new Date() > this.expirationDate);
  }
}
