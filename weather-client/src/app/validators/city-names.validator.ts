import { FormControl } from '@angular/forms';

export class CityValidators {
  public static RestrictDigits(control: FormControl): { [s: string]: boolean } {
    const regex = /\d/;
    if (regex.test(control.value)) {
      return {cityHasDigits: true};
    }
    return null;
  }
}
