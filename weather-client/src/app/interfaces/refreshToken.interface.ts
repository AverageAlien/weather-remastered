export interface RefreshToken {
  expiredToken: string;
  refreshToken: string;
}
