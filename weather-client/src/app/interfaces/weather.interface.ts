export interface WeatherResponse {
  cityName: string;
  temperature: number;
  weatherStatus: string;
  icon: string;
  date: string;
}

export interface Weather {
  town: string;
  temp: number;
  weatherType: string;
  icon: string;
  date: Date;
}
