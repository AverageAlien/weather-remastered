export interface TokenResponse {
  token: string;
  username: string;
  expirationDate: Date;
  refreshToken: string;
}
